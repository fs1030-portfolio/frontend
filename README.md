# Chris Germishuys Frontend

# Run this project using docker

1. You can run this project using docker build and docker run
2. Ensure that you have first set up the backend project (https://gitlab.com/fs1030-portfolio/backend)
3. Install docker if you don't have it already
4. In the Dockerfile, update the REACT_APP_API to http://localhost:4000
5. Use the following commands to run the react app

docker build -t frontend .
docker run -d -p  3000:3000 frontend (you can change the first port to an available port on your system)

6. Load up localhost:3000 in your browser and it should now show the frontend

# Run this project using npm

1. To run this project you will need to clone this repo using SSH or HTTP
2. Open the folder in VS Code or applicable source-code editor with access to terminal
3. When in the terminal, use npm install to install node dependencies
4. The project uses http://localhost:3000 to run so ensure that port 3000 is not in use
5. In the terminal, type in 'npm start' and go to http://localhost:3000 in your preferred browser (it should open automatically)
6. From here you can Navigate the React App
7. To login as an administrator for the site use the following username and password:

Username: chris@chris.com
Password: somepassword

# To fully utilize this, make sure to run the node backend that is included in this group and follow the instructions there (Remember to update the port for the backend to 4000)
