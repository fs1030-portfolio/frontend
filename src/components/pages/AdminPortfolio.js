import { useEffect, useState } from "react";
import { Button, Container, Table } from "react-bootstrap";
import { useHistory } from "react-router-dom";
import parseJwt from "../../helpers/authHelper";

const AdminPortfolio = () => {
  const token = sessionStorage.getItem("token");
  const user = parseJwt(token).username;
  const [portfolio, setPortfolio] = useState([]);
  const history = useHistory()

  const getData = async () => {
    const response = await fetch(
      process.env.REACT_APP_API + `/admin/portfolio`,
      {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
        },
      }
    );
    const data = await response.json();
    setPortfolio(data);
  };
  
  // eslint-disable-next-line
  useEffect(() => {getData();}, [token]);

  const editPortfolio = (event, portfolio) => {
    event.preventDefault();
    let path = `/admin/portfolio/edit/${portfolio.id}`
    history.push(path, portfolio)
  }

  const addPortfolio = (event, portfolio) => {
    event.preventDefault();
    let path = `/admin/portfolio/add`
    history.push(path, portfolio)
  }

  const deletePortfolio = async (event, portfolio) => {
    event.preventDefault();
    await fetch(process.env.REACT_APP_API + `/admin/portfolio/delete/${portfolio.id}`, {
      'method': 'DELETE',
      'headers': {
        'Authorization': `Bearer ${token}`,
        'Content-Type': 'application/json'
      }
    })
    getData()
  }

  return (
    <Container className="p-5">
      <h2 className="text-center">Portfolio Administration Panel</h2>
      <p>You are logged in as: {user}</p>
      <p>Use this page to modify and add entries on the portfolio page.</p>
      <Table className="my-2" striped bordered hover responsive>
        <thead>
          <tr>
            <th>#</th>
            <th>Title</th>
            <th>Content</th>
            <th>URL</th>
            <th></th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          {portfolio.length === 0 && (
            <tr>
              <td colSpan="4" className="text-center">
                <i>No entries found</i>
              </td>
            </tr>
          )}
          {portfolio.length > 0 &&
            portfolio.map((portfolio) => (
              <tr key={portfolio.id}>
                <td>{portfolio.id}</td>
                <td>{portfolio.title}</td>
                <td>{portfolio.content}</td>
                <td>{portfolio.url}</td>
                <td className="text-center"><Button variant="outline-primary" onClick={(e) => editPortfolio(e, portfolio)}>Edit</Button></td>
                <td className="text-center"><Button variant="outline-danger" onClick={(e) => deletePortfolio(e, portfolio)}>Delete</Button></td>
              </tr>
            ))}
        </tbody>
      </Table>
      <Button variant="outline-primary" onClick={(e) => addPortfolio(e, portfolio)}>Add Entry</Button>
    </Container>
  );
};

export default AdminPortfolio;
