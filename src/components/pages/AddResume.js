import { useState } from "react";
import { Button, Container, Form, InputGroup } from "react-bootstrap";
import { useHistory } from "react-router-dom";

const AddResume = (props) => {
  const token = sessionStorage.getItem("token");
  const history = useHistory();
  const [message, setMessage] = useState("");
  const [resume, setResume] = useState();

  const formSubmit = async (event) => {
    event.preventDefault();
    const response = await fetch(process.env.REACT_APP_API + `/admin/resume/add/`, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify(resume),
    });
    const payload = await response.json();
    if (response.status >= 400) {
      setMessage(
        `Please fix the following input fields to submit the form: ${payload}
        )}`
      );
    } else {
      history.push("/admin/resume");
    }
  };

  const handleAdd = (event) => {
    event.persist();
    setResume((prevState) => ({
      ...prevState,
      [event.target.name]: event.target.value,
    }));
  };

  return (
    <Container className="p-5">
      <h2 className="text-center">Use this page to add a resume entry.</h2>
      <p className="text-center">
        <strong>{message}</strong>
      </p>{" "}
      {/* This will display the message based on the response from the backend. Would like to improve this in future to be per field and highlight correctly */}
      <Form onSubmit={(e) => formSubmit(e)}>
        <Form.Label>Title</Form.Label>
        <InputGroup className="mb-3">
          <Form.Control
            type="text"
            name="title"
            required
            onChange={handleAdd}
          />
        </InputGroup>
        <Form.Label>Content</Form.Label>
        <InputGroup className="mb-3">
          <Form.Control
            type="text"
            name="content"
            as="textarea"
            required
            rows={10}
            onChange={handleAdd}
          />
        </InputGroup>
        <Button type="submit" variant="outline-primary">
          Submit
        </Button>
      </Form>
    </Container>
  );
};

export default AddResume;
