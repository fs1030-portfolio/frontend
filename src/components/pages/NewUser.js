import { useState } from "react";
import { Container, Button, Form, Col } from "react-bootstrap";

const NewUser = () => {
  const token = sessionStorage.getItem("token");
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [message, setMessage] = useState("");

  // Submit function that will add a new user to the backend
  // This will allow the new user to login with their credentials
  const formSubmit = async (event) => {
    event.preventDefault();
    const response = await fetch(process.env.REACT_APP_API + `/users`, {
      method: "POST",
      headers: {
        "Accept": "application/json",
        "Content-Type": "application/json",
        "Authorization": `Bearer ${token}`
      },
      body: JSON.stringify({ name, email, password }),
    });
    const payload = await response.json();
    if (response.status >= 400) {
      setMessage(
        `Please fix the following input fields to submit the form: ${payload.message}`);
    } else {
      setMessage(
        `Congratulations! You have created a new user. Please keep your password in a safe place.`
      );
      setName("");
      setEmail("");
      setPassword("");
    }
  };

  return (
    <Container className="p-5">
      <Form onSubmit={formSubmit}>
        <h1 className="text-center">Add New User</h1>
        <p className="text-center">
          Please use this page add new users to the site.
        </p>
        <p className="text-center">
          <strong>{message}</strong> {/* This will display the message from the backend depending on failure or success */}
        </p>
        <Form.Group className="mb-3">
          <Form.Label>Email address</Form.Label>
          <Form.Control
            type="email"
            name="email"
            id="emailEntry"
            value={email}
            required
            onChange={(e) => setEmail(e.target.value)}
            placeholder="Enter email address..."
          />
          <Form.Text className="text-muted">
            We'll never share your email with anyone else.
          </Form.Text>
        </Form.Group>
        <Form.Group className="mb-3">
          <Form.Label>Full Name</Form.Label>
          <Form.Control
            type="name"
            name="name"
            id="nameEntry"
            value={name}
            required
            onChange={(e) => setName(e.target.value)}
            placeholder="Enter full name..."
          />
        </Form.Group>
        <Form.Group className="mb-3">
          <Form.Label>Password</Form.Label>
          <Form.Control
            type="password"
            name="password"
            id="passwordEntry"
            value={password}
            required
            onChange={(e) => setPassword(e.target.value)}
            placeholder="Please enter a password that is atleast 8 characters long..."
          />
        </Form.Group>
        <Form.Group>
          <Col>
            <Button variant="outline-primary" type="submit">
              Submit
            </Button>
          </Col>
        </Form.Group>
      </Form>
    </Container>
  );
};

export default NewUser;
