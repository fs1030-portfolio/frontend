import { useState } from "react";
import { Container, Button, Form, Card, Col } from "react-bootstrap";
import { useHistory, useLocation } from "react-router-dom";

const Login = (props) => {
  let history = useHistory();
  let location = useLocation();
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [loginFailure, setLoginFailure] = useState(false);

  const loginSubmit = async (event) => {
    event.preventDefault();
    const response = await fetch(process.env.REACT_APP_API + `/auth`, {
      method: "POST",
      headers: {
        "Accept" : "application/json",
        "Content-Type" : "application/json",
      },
      body: JSON.stringify({ email, password }),
    });
    const payload = await response.json();
    if (response.status >= 400) {
      setLoginFailure(true);
    } else {
      props.setAuth(true);
      sessionStorage.setItem("token", payload.token);

      let { from } = location.state || { from: { pathname: "/admin/submissions" } };
      history.replace(from);
    }
  };

  return (
    <Container className="p-5">
      <Col md={{ span: 12, offset: 3 }}>
        <h1>Login</h1>
        <p>Enter your e-mail address and password to login</p>
        {loginFailure && (
          <Card className="text-white bg-danger my-5 text-center col-sm-5">
            <Card.Body>
              <Card.Text className="text-white">
                Invalid credentials, please try again
              </Card.Text>
            </Card.Body>
          </Card>
        )}
      </Col>
      <Col md={{ span: 12, offset: 3 }}>
        <Form onSubmit={loginSubmit}>
          <Form.Group className="mb-3 col-sm-5">
            <Form.Label>Email Address</Form.Label>
            <Form.Control
              type="text"
              name="email"
              id="emailEntry"
              placeholder="Email"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
            />
          </Form.Group>
          <Form.Group className="mb-3 col-sm-5">
            <Form.Label>Password</Form.Label>
            <Form.Control
              type="password"
              name="password"
              id="passwordEntry"
              placeholder="Password"
              onChange={(e) => setPassword(e.target.value)}
            />
          </Form.Group>
          <Form.Group>
            <Button variant="primary" type="submit">
              Submit
            </Button>
          </Form.Group>
        </Form>
      </Col>
    </Container>
  );
};

export default Login;
