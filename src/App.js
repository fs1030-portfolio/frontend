import "./App.css";
import Navigation from "./components/shared/Navigation";
import Footer from "./components/shared/Footer";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import { useState, useEffect } from "react";
import Home from "./components/pages/Home";
import Resume from "./components/pages/Resume";
import Portfolio from "./components/pages/Portfolio";
import Contact from "./components/pages/Contact";
import Login from "./components/pages/Login";
import AdminSubmissions from "./components/pages/AdminSubmissions";
import NewUser from "./components/pages/NewUser";
import AdminResume from "./components/pages/AdminResume";
import AdminPortfolio from "./components/pages/AdminPortfolio";
import EditResume from "./components/pages/EditResume";
import PrivateRoute from "./components/shared/PrivateRoute";
import isAuthenticated from "./helpers/authHelper";
import AddResume from "./components/pages/AddResume";
import AddPortfolio from "./components/pages/AddPortfolio";
import EditPortfolio from "./components/pages/EditPortfolio";
import ViewSubmission from "./components/pages/ViewSubmission";
import authVerify from "./helpers/authVerify";

function App() {
  // The default login state is false
  // useEffect hook is used to run the isAuthenticated function
  // This will then setAuth either to true or false depending on whether there is a user logged in or not
  // Currently looking for a better method to handle expired token and redirect to logout
  const [auth, setAuth] = useState(false);
  useEffect(() => {
    if (isAuthenticated() && authVerify()) {
      setAuth(true)
    } else {
      setAuth(false)
    }
  }, []);

  return (
    <BrowserRouter>
      <Navigation auth={auth} setAuth={setAuth} /> {/* Making props available to Navigation to return correct menu items based on auth */}
      <Switch>
        <Route exact path="/" component={Home} />
        <Route exact path="/resume" component={Resume} />
        <Route exact path="/portfolio" component={Portfolio} />
        <Route exact path="/contact" component={Contact} />
        <Route exact path="/login" component={() => <Login auth={auth} setAuth={setAuth} />} /> {/* Making props available to Login to return state of true or false for auth */}
        <PrivateRoute exact path="/admin/submissions" component={AdminSubmissions} />
        <PrivateRoute exact path="/admin/submissions/view/:id" component={ViewSubmission} />
        <PrivateRoute exact path="/admin/user" component={NewUser} />
        <PrivateRoute exact path="/admin/resume" component={AdminResume} />
        <PrivateRoute exact path="/admin/resume/add" component={AddResume} />
        <PrivateRoute exact path="/admin/resume/edit/:id" component={EditResume} />
        <PrivateRoute exact path="/admin/portfolio" component={AdminPortfolio} />
        <PrivateRoute exact path="/admin/portfolio/add" component={AddPortfolio} />
        <PrivateRoute exact path="/admin/portfolio/edit/:id" component={EditPortfolio} />
      </Switch>
      <Footer />
    </BrowserRouter>
  );
}

export default App;